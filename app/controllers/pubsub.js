'use strict';

// server-side socket behaviour
// io is a variable already taken in express
var redis = require("redis");
var util = require('bitcore/util/util');
var pub = redis.createClient();
var sub = redis.createClient();

var Address = require('../models/Address');

module.exports.init = function(app) {



};

module.exports.broadcastTx = function(tx) {
    var t;
    if (typeof tx === 'string') {
      t = {
        txid: tx
      };
    }
    
    else {
      t = {
        txid: tx.txid,
        size: tx.size,
      };
      // Outputs
      var valueOut = 0;
      tx.vout.forEach(function(o) {
        valueOut += o.value * util.COIN;
      });

      t.valueOut = parseInt(valueOut) / util.COIN;
    }

    pub.publish('tx', JSON.stringify(t));
};

module.exports.broadcastBlock = function(block) {
  pub.publish('block', block);
};

module.exports.broadcastAddressTx = function(addr, tx) {

	pub.publish(addr, tx);

	/*var address = new Address(addr);

  address.update(function(error) {
    if(!error) {
      pub.publish(addr, tx);
      pub.hmset(addr, address);
    } else {
      // error updating address
      pub.publish(addr, tx);
    }
  });*/

};

module.exports.broadcastSyncInfo = function(historicSync) {
  pub.publish('status', historicSync);

};
